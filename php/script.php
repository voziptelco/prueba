<?php
/**
 * Created by PhpStorm.
 * User: ramses
 * Date: 22/01/20
 * Time: 09:55 AM
 */

$colores_vehiculos = array(
    'coche' => 'rojo',
    'moto' => 'verde',
    'avion' => 'amarillo'
);

// Consultamos un valor en un indice o llave en especifico
echo $colores_vehiculos['moto'];

// Cambiar un valor de la llave coche a color azul
$colores_vehiculos['coche'] = 'azul';
print_r($colores_vehiculos);
echo '<br>';

// Eliminar el elemento avion
unset($colores_vehiculos['avion']);
print_r($colores_vehiculos);


