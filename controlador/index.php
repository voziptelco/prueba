<?php

    require '../vendors/lib_urls.php';
    require '../vendors/lib_page.php';

    Html::initHtml();
    if(isset($_GET) && isset($_GET['item'])){
        route($_GET['item']);
    }
    elseif(isset($_GET) && isset($_GET['error'])) {
        require 'cError.php';
        getError($_GET['error']);
    }
    else{
        error('404');
    }
    Html::endHTML();
    
