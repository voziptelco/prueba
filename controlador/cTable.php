<?php

    require '../vendors/lib_db.php';

    if(isset($_GET) && isset($_GET['id'])){
        $conexion = new Conexion(HOST,USERNAME,PASSWORD,DATABASE);
        $objCRUD = new CRUD_Contactos($conexion->getConexion());
        $objCRUD->eliminarContacto($_GET['id']);
        header('location: http://localhost/prueba/controlador/index.php?item=table');
    }
    else{
        $conexion = new Conexion(HOST,USERNAME,PASSWORD,DATABASE);
        $objCRUD = new CRUD_Contactos($conexion->getConexion());
        $resultados = $objCRUD->consultaContactos();
        include '../plantilla/table/table.php';
    }
