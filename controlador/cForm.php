<?php

    require '../vendors/lib_db.php';
    include '../plantilla/form/form.php';

    if(isset($_POST) && isset($_POST['NOMBRE']) && isset($_POST['APELLIDOS']) && isset($_POST['DIRECCION']) ){
        if($_POST['NOMBRE'] != ''){
            $conexion = new Conexion(HOST,USERNAME,PASSWORD,DATABASE);
            $objCRUD = new CRUD_Contactos($conexion->getConexion());
            $objCRUD->agregarContacto($_POST);
            header('location: http://localhost/prueba/controlador/index.php?item=table');
        }
        else {
            header('location: http://localhost/prueba/controlador/index.php?error=form');
        }
    }
