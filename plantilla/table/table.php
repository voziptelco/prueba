<main class="flex-shrink-0">
    <div class="container">
        <table class="table table-striped">
            <!-- Encabezado Inicio -->
            <thead>
            <tr>
                <th>#</th>
                <th>Nombre</th>
                <th>Apellidos</th>
                <th>Direccion</th>
                <th>Acciones</th>
            </tr>
            </thead>
            <!-- Encabezado Fin -->
            <!-- Datos de la tabla -->
            <tbody>
            <?php
                while ($registro = mysqli_fetch_array($resultados, MYSQLI_ASSOC)) {
                    echo '<tr>';
                    echo '<th scope="row">1</th>';
                    echo "<td>" . $registro['NOMBRE'] . "</td>";
                    echo "<td>" . $registro['APELLIDOS'] . "</td>";
                    echo "<td>" . $registro['DIRECCION'] . "</td>";
                    echo "<td><a class='btn btn-primary' href='cTable.php?id=" . $registro['ID'] ."'>Eliminar</a></td>";
                }
            ?>
            </tbody>
        </table>
    </div>
</main>
