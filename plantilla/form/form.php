<main class="flex-shrink-0">
    <div class="container">

        <!-- Contenido -->
        <form action="cForm.php" method="post">
            <div class="form-group">
                <label>Nombre</label>
                <input type="text" name="NOMBRE" class="form-control" placeholder="Nombre">
            </div>

            <div class="form-group">
                <label>Apellidos</label>
                <input type="text" name="APELLIDOS" class="form-control" placeholder="Apellidos" >
            </div>

            <div class="form-group">
                <label>Direccion</label>
                <input type="text" name="DIRECCION" class="form-control" placeholder="Direccion">
            </div>

            <button type="submit" class="btn btn-primary">Guardar</button>
        </form>

    </div>
</main>