<?php

    require 'config.php';

    /**
     * Class Conexion
     */
    class Conexion {

        /**
         * @var conexion
         */
        var $conexion;

        /**
         * Conexion constructor.
         *
         * @param $host
         * @param $username
         * @param $password
         * @param $database
         */
        function __construct($host, $username, $password, $database){
            $this->conexion = mysqli_connect($host, $username, $password, $database);
        }

        function __call($name, $arguments)
        {
            // TODO: Implement __call() method.
        }

        public function getConexion(){
            return $this->conexion;
        }
    }

    class CRUD_Contactos {

        var $conexion;

        function __construct($conexion){
            $this->conexion = $conexion;
        }

        function __destruct(){
            mysqli_close($this->conexion);
        }

        public function consultaContactos(){
            $sql = 'SELECT * FROM TBL_CONTACTOS WHERE STATUS=1';
            $resultado = mysqli_query($this->conexion, $sql);
            return $resultado;
        }

        public function agregarContacto($datos){
            if(is_array($datos) && isset($datos['NOMBRE']) && isset($datos['APELLIDOS']) && isset($datos['DIRECCION'])){
                if($datos['NOMBRE']!='' && $datos['APELLIDOS']!=''){
                    $sql = "INSERT INTO TBL_CONTACTOS (NOMBRE, APELLIDOS, DIRECCION) VALUES ('" . $datos['NOMBRE'] . "', '" . $datos['APELLIDOS'] . "', '" . $datos['DIRECCION'] . "')";
                    mysqli_query($this-> conexion, $sql);
                }
            }
        }

        public function eliminarContacto($idRegistro = false){
            if($idRegistro){
                $sql = "UPDATE TBL_CONTACTOS SET STATUS = 0 WHERE ID = $idRegistro";
                mysqli_query($this->conexion, $sql);
            }
        }

    }