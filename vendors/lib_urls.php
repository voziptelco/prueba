<?php

    function route($item){
        switch ($item){
            case 'home':
                include 'cHome.php';
                break;
            case 'form':
                include 'cForm.php';
                break;
            case 'table':
                include 'cTable.php';
                break;
            default:
                include 'cHome.php';
                break;
        }
    }

    function error($error){
        require 'cError.php';
        getError($error);
    }
